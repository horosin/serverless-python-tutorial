# AWS Lambda and API Gateway - getting started with serverless REST APIs by Karol Horosin

Serverless framework tutorial focused on the development process and getting started with the framework

## Resources

Video - [facebook video](https://www.facebook.com/Ardigen.SA/videos/552043545473567)

My newsletter - I am sending my other learning resources and conference talk recordings: [sign up](https://mailchi.mp/a1db5198f63f/karol-horosin-conference-newsletter)


Serverless Stack Tutorial (including React frontend) - https://serverless-stack.com/


My other tutorial covering the same topic but using JS/node: [gitlab](https://gitlab.com/horosin/serverless-js-tutorial
)