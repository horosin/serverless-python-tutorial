import json
from core.lib import lib_fun


def hello(event, context):
    body = {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "input": event
    }

    response = {
        "statusCode": 200,
        "body": {
            "message": lib_fun()
        }
    }

    return response
